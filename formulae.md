# Custom Catan Formulae

> "20cc" means test was conducted by mixing pigments into 20cc total, 10cc each
> of parts A and B.

  - TODO
    + Player pieces
    + Resources
    + Hills variations
    + Oceans variations

## Pasture (325)

  - Final (X3, 10cc)
    + 10 drops yellow dye
    + 1/2 part forest mica
    + 1 part chartreuse mica

  - X1 (10cc)
    + 5 drops yellow dye
    + 1 part forest mica
    + 1 part chartreuse mica

  - X2 (10cc)
    + 10 drops yellow dye
    + 1 part forest mica
    + 1 part chartreuse mica

## Pasture (300)

  - Final (10cc)
    + 1 drop green dye
    + 8 drops yellow dye

## Forest (325)

  - Final (D2', 40cc)
    + 1 drop blue dye
    + 2 drops yellow dye
    + 50mg forest mica

## Forest (300)

  - Final (10cc)
    + 10 drops yellow
    + 10 drops green
    + 1/2 drop black

## Hills (325)

  - TODO
    + More variations

  - Final (E2, 20cc)
    + 1 drop yellow dye
    + 100mg wine mica

  - Original
    + 1 part coper mica
    + 2 parts wine mica
    + 4 parts chestnut mica
    + 4+ parts brown mica

## Hills (300)

  - Final (10cc)
    + 3 drops flesh
    + 3 drops yellow
    + 2 drops red
    + 1 drop brown

## Plains (325)

  - Final (10cc)
    + 2 parts gold mica
    + 1 part pearl mica
    + Pinch chestnut mica
    + *Vary amount of chestnut for variety*

  - E7 (20cc)
    + 1/2 drop brown dye
    + 100mg gold mica
    + 100mg pearl mica

  - E10 (20cc)
    + 100mg gold mica
    + 100mg chestnut mica
    + 100mg pearl mica

  - X1 (10cc)
    + 2 parts gold mica
    + 3-4 parts pearl mica

  - X2 (10cc)
    + 1 part gold mica
    + 1 part pearl mica

## Plains (300)

  - Final (10cc)
    + 5 drops brown dye
    + 5 drops red dye
    + 8 drops yellow dye
    + In 5ml part B
    + Dilute 16-to-1

## Mountain (325)

  - Original
    + Grey and black mica (unknown ratio, between 4:1 and 2:1)

  - Original'
    + Grey mica
    + Add Black and Pearl mica right before mixing

  - Original''
    + Grey mica and black or pearl mica, separately
    + Marble

## Mountain (300)

  - Final (10cc)
    + 1 drop black

## Desert (325)

  - Final (10cc)
    + 1 part gold mica
    + 2-3 parts pearl mica
    + 1 part bronze mica
    + *By weight*

  - E11 (20cc)
    + 100mg pearl mica
    + 50mg gold mica
    + 50mg bronze mica
    + 50mg grey mica

## Desert (300)

  - Final (10cc)
    + 1 drop black dye
    + 1 drop brown dye
    + 1 drop yellow dye
    + In 1ml part B
    + Dilute 4-to-1

## Ocean (325)

  - Final (10cc)
    + 1 drop blue dye
    + 50-100mg blueberry mica

  - E8 (40cc)
    + 1 drop blue dye
    + 100mg blueberry mica
    + 50mg violet mica

  - E8' (40cc)
    + 1 drop blue dye
    + 100mg blueberry mica

  - E8'' (40cc)
    + 2 drops blue dye
    + 50mg blueberry mica