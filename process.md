# Custom Catan Process

## Molds

### Ring

  1. Print ring model + box
  2. Mix and pour silicone
     + Optionally top with acrylic (cut channels for excess to escape)
  3. Remove mold from box when cured

### Hex

  1. Print hex base
  2. Cut rough shape of top out of acrylic
  3. Refine to the size of the indentation by filing and sanding
     + Optionally flame polish edges (can cause interesting warping)
  4. Attach top to base (with CA glue)
  5. Seal edges between top and base with CA glue
  6. Wait minimum of 2 days for CA glue to totally cure
     + Partially cured CA glue can inhibit cure of silicone
  7. Mix and pour silicone
     + Optionally top with acrylic (cut channels for excess to escape)
  8. Remove mold from box when cured

### Resources

  1. Attach square of contact paper to something flat, sticky side up
  2. Place Xia resource cubes on contact paper
  3. Attach sides to make a mold box
  4. Mix and pour silicone
     + Optionally top with acrylic (cut channels for excess to escape)
  5. Remove mold from box when cured
  6. Remove any cubes that stayed in the mold

### Player Pieces

  1. Carve out of hard wax
  2. Make mold (same method as Resources)
  3. Cast master
  4. Make more molds

## Materials

  + Ring Mold
  + Hex Mold
  + Resources Mold
  + Player Pieces Mold
  + Smooth-Cast 300
  + Smooth-Cast 325
  + Smooth-Cast 327 (?)
  + SO-Strong Color Tint
  + Mica Powder Pigment
  + K&J Magnetics DH11 (disc, 1/10" x 1/16", N32, Ni)
  + Acrylic for covering casts

## Tools

  + Scale for measuring mica (milligram accuracy)
  + Scoops for mica
  + Glass stirring rods for mixing resin (NOT FOR SILICONE)
  + Disposable cups for mixing resin
  + Gloves (resin is not good for you)
  + Protective clothing (resin does not wash out and is not good for you)
  + Ventilation or a ventilator

## Instructions

Smooth-Cast 300 and 325 cure very quickly. Their stated pot life (the duration
after mixing where the mixture will remain workable) is less than 3 minutes. As
such, vacuum degassing the final mixture is problematic. However, vacuum
degassing part B after mixing in pigments can help reduce bubbles.

Casting resources and player pieces becomes difficult or impossible as the resin
becomes more viscous. Thus, if the mixture starts getting hot, it should be used
immediately. Casting the hexes is far more forgiving and can still be done even
as the resin is setting. Of course, waiting too long will still cause problems.

### Resources

  1. Add 5ml 325 part B into disposable cup
  2. Add pigment, mix thoroughly
  3. Add 5ml 325 part A, **mix thoroughly**
     + If the mixture is noticeably warm/hot, it will soon be unworkable
  4. Fill 10cc syringe and dispense into resource mold
     + Carefully fill each cube almost to the top, leaving a concave meniscus (resin will expand)
  5. After fully cured (resin does not deform, ~20 min), remove from mold

### Player Pieces

Probably mostly the same as for Resources. Maybe use 327 and vacuum degas?

### Hexes

Instructions are for a single hex. Making multiple hexes at once will reduce the
number of steps and simplify pigmentation but increases the chance of human
error.

When pouring the first layer of the hex, the ring can be inserted after the
resin, instead of before. This simplifies the process, but it results in a
different aesthetic.

Be careful when handling the rings. The magnets are not affixed strongly and may
pop out. If one does, glue it back in place (ensuring the correct polarity) with
CA glue.

  1. Ring
     1. Insert 12 magnets into slots in mold, alternating polarity
        + **Magnets must be oriented identically on all hexes**
     2. Add 2ml 300 part B into disposable cup
     3. Add pigment, mix thoroughly
     4. Add 2ml 300 part A, **mix thoroughly**
        + If the mixture is noticeably warm/hot, it will soon be unworkable
     5. Pour into mold
        + Using a syringe can simplify this
        + Optionally top with acrylic for a smoother surface
     6. After fully cured (ring is rigid and does not deform, ~30 min), remove from mold

  2. Hex
     1. Insert ring into mold
        + Carefully check to ensure the ring is properly seated
     2. Add 10ml 325 part B into disposable cup
     3. Add pigment, mix thoroughly
     4. Add 10ml 325 part A, **mix thoroughly**
        + If mixture is noticeably warm/hot, pour soon/immediately
     5. Pour into the mold
        + Tilt the mold to ensure there are no air bubbles left under the ring
     6. Add 5ml 300 part B into disposable cup
     7. Add pigment, mix thoroughly
     8. Add 5ml 300 part A, **mix thoroughly**
        + If mixture is noticeably warm/hot, pour soon/immediately
        + Optionally top with acrylic for a smoother surface
     9. After fully cured (hex is rigid and does not deform, ~30 min), remove from mold

## Kit

  + Molds
  + SO-Strong
  + Mica
  + Magnets
  + Scoop
  + Glass rod

Not in kit:

  + Resin
  + Acrylic
  + Scale
  + Disposable cups
  + Gloves